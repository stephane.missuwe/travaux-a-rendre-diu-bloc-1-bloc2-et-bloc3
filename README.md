# Travaux à rendre DIU  bloc 1 bloc2 et bloc3

Les travaux à rendre pour le 13 decembre 2021 sont décrit ci-dessous et 
envoyés sous forme de fichier Zip

Bloc 1: le fichier zip missuwe bloc1.zip contient
    - Le programme wator : missuwe_programme_wator.py
    - Les suites pegagogiques possibles : Missuwe projection sequence et theme wator.docx

Bloc 2: le fichier zip missuwe bloc 2.zip contient
    - un projet sur les algorithmes gloutons constitué
        - du programme python de correction : Missuwe programme gestion stock glouton bloc 2.py
                                                avec un fichier CSV : liste_medicament.csv 
        - 2 fichiers prof et eleve du projet
            * Missuwe gestion de stock de médicament eleve bloc 2.docx
            * Missuwe gestion de stock de médicament prof bloc 2.docx
        - d'un document technique sur les algorithmes
            *Missuwe document technique algorithme glouton bloc 2.docx

Bloc 3: le fichier zip missuwe bloc1.zip contient 
    - Un Tp sur les réseaux et routeur sous filius eleve et prof
        * Missuwe Tp réseau bloc 3 eleves.docx
        * Missuwe Tp réseau bloc 3 prof.docx
     - un document sur les protocoles de routage
        * Missuwe Doc protocole bloc3 .docx
     - 8 fichiers filius prof et eleves

    
-
